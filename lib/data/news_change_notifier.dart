import 'package:flutter/cupertino.dart';
import 'package:testing_flutter_apps/data/news_service.dart';
import 'package:testing_flutter_apps/models/article.dart';

class NewsChangeNotifier extends ChangeNotifier {
  final NewsService _newsService;

  NewsChangeNotifier(this._newsService);

  List<Article> _articles = [];
  List<Article> get articles => _articles;

  bool _isLoading = false;
  bool get isLoading => _isLoading;

  Future<void> getArticles() async {
    _isLoading = true;
    notifyListeners();
    _articles =  await _newsService.getArticles();
    _isLoading = false;
    notifyListeners();
  }
}
