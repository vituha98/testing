import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:provider/provider.dart';
import 'package:testing_flutter_apps/data/news_change_notifier.dart';
import 'package:testing_flutter_apps/data/news_service.dart';
import 'package:testing_flutter_apps/main.dart';
import 'package:testing_flutter_apps/models/article.dart';
import 'package:testing_flutter_apps/pages/news_page.dart';

class MockNewsService extends Mock implements NewsService {}

void main() {
  late MockNewsService mockNewsService;

  setUp(() {
    mockNewsService = MockNewsService();
  });

  final articlesFromService = [
    Article(title: 'title 1', content: 'Test 1 content'),
    Article(title: 'title 2', content: 'Test 2 content'),
    Article(title: 'title 3', content: 'Test 3 content'),
  ];

  void arrangeNewsSeviceReturns3Articles() {
    when(() => mockNewsService.getArticles())
        .thenAnswer((_) async => articlesFromService);
  }

  void arrangeNewsSeviceReturns3ArticlesAfter2SecondWait() {
    when(() => mockNewsService.getArticles()).thenAnswer((_) async {
      await Future.delayed(const Duration(seconds: 2));
      return articlesFromService;
    });
  }

  Widget createWidgetUnderTest() {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'News App',
      home: ChangeNotifierProvider(
        create: (_) => NewsChangeNotifier(mockNewsService),
        child: NewsPage(),
      ),
    );
  }

  testWidgets('title is displayed', (tester) async {
    arrangeNewsSeviceReturns3Articles();
    await tester.pumpWidget(createWidgetUnderTest());
    expect(find.text('News'), findsOneWidget);
  });

  testWidgets(
    'loading indicator is displayed while waiting for articles',
    (WidgetTester tester) async {
      arrangeNewsSeviceReturns3ArticlesAfter2SecondWait();
      await tester.pumpWidget(createWidgetUnderTest());
      await tester.pump(const Duration(milliseconds: 500));
      expect(find.byKey(Key('progress-indicator')), findsOneWidget);
      await tester.pumpAndSettle();
    },
  );

  testWidgets('articles are displayed', (WidgetTester tester) async {
    arrangeNewsSeviceReturns3Articles();
    await tester.pumpWidget(createWidgetUnderTest());
    await tester.pump();
    for (final article in articlesFromService) {
      expect(find.text(article.title), findsOneWidget);
      expect(find.text(article.content), findsOneWidget);
    }
  });
}
