import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:testing_flutter_apps/data/news_change_notifier.dart';
import 'package:testing_flutter_apps/data/news_service.dart';
import 'package:testing_flutter_apps/models/article.dart';

class MockNewsService extends Mock implements NewsService {}

void main() {
  late NewsChangeNotifier sut; //system under test
  late MockNewsService mockNewsService;
  setUp(() {
    mockNewsService = MockNewsService();
    sut = NewsChangeNotifier(mockNewsService);
  });

  test('initial values are correct', () {
    expect(sut.articles, []);
    expect(sut.isLoading, false);
  });

  group('getArticles', () {
    final articlesFromService = [
            Article(title: 'title 1', content: 'Test 1 content'),
            Article(title: 'title 2', content: 'Test 2 content'),
            Article(title: 'title 3', content: 'Test 3 content'),
          ];
    void arrangeNewsSeviceReturns3Articles() {
      when(() => mockNewsService.getArticles()).thenAnswer((_) async => articlesFromService);
    }

    test('gets articles using the NewsService', () async {
      arrangeNewsSeviceReturns3Articles();
      await sut.getArticles();
      verify(() => mockNewsService.getArticles()).called(1);
    });

    test('''indicates loading of data, 
    sets articles to the ones from the service,
    indicates that data is not being loaded anymore''', () async {
      arrangeNewsSeviceReturns3Articles();
      final future = sut.getArticles();
      expect(sut.isLoading, true);
      await future;
      expect(sut.articles, articlesFromService);
      expect(sut.isLoading, false);
    });
  });
}
