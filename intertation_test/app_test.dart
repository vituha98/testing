import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:provider/provider.dart';
import 'package:testing_flutter_apps/data/news_change_notifier.dart';
import 'package:testing_flutter_apps/data/news_service.dart';
import 'package:testing_flutter_apps/main.dart';
import 'package:testing_flutter_apps/models/article.dart';
import 'package:testing_flutter_apps/pages/article_page.dart';
import 'package:testing_flutter_apps/pages/news_page.dart';

class MockNewsService extends Mock implements NewsService {}

void main() {
  late MockNewsService mockNewsService;

  setUp(() {
    mockNewsService = MockNewsService();
  });

  final articlesFromService = [
    Article(title: 'Test 1', content: 'Test 1 content'),
    Article(title: 'Test 2', content: 'Test 2 content'),
    Article(title: 'Test 3', content: 'Test 3 content'),
  ];

  void arrangeNewsSeviceReturns3Articles() {
    when(() => mockNewsService.getArticles())
        .thenAnswer((_) async => articlesFromService);
  }

  Widget createWidgetUnderTest() {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'News App',
      home: ChangeNotifierProvider(
        create: (_) => NewsChangeNotifier(mockNewsService),
        child: NewsPage(),
      ),
    );
  }

  testWidgets("""Tapping on the first article excerpt opens the article page
  where the full article content is displayed""", (tester) async {
    arrangeNewsSeviceReturns3Articles();

    await tester.pumpWidget(createWidgetUnderTest());
    await tester.pump();
    await tester.tap(find.text('Test 1 content'));
    await tester.pumpAndSettle();
    expect(find.byType(NewsPage), findsNothing);
    expect(find.byType(ArticlePage), findsOneWidget);
    expect(find.text('Test 1'), findsOneWidget);
    expect(find.text('Test 1 content'), findsOneWidget);
  });
}
